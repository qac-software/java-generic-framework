package qac;

import com.qaconsultants.misc.DriverFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TestDriverFactory
{
    @BeforeClass
    public static void before()
    {
        System.setProperty("qac.webdriverDirectory", "./resources/webdrivers/");
    }

   // @Test
    public void createLocalChromeDriverFromString() throws Exception
    {
        WebDriver _driver = DriverFactory.getLocalDriver("CHROME");
        assert(_driver instanceof ChromeDriver);
        _driver.quit();
    }

   // @Test
    public void createLocalChromeDriverFromEnum() throws Exception
    {
        WebDriver _driver = DriverFactory.getLocalDriver(DriverFactory.Browser.CHROME);
        assert(_driver instanceof ChromeDriver);
        _driver.quit();
    }

   // @Test
    public void createLocalFirefoxDriverFromString() throws Exception
    {
        WebDriver _driver = DriverFactory.getLocalDriver("FIREFOX");
        assert(_driver instanceof FirefoxDriver);
        _driver.quit();
    }

    //@Test
    public void createLocalFirefoxDriverFromEnum() throws Exception
    {
        WebDriver _driver = DriverFactory.getLocalDriver(DriverFactory.Browser.FIREFOX);
        assert(_driver instanceof FirefoxDriver);
        _driver.quit();
    }

    //@Test
    public void createLocalIEDriverFromString() throws Exception
    {
        WebDriver _driver = DriverFactory.getLocalDriver("IE");
        assert(_driver instanceof InternetExplorerDriver);
        _driver.quit();
    }

   // @Test
    public void createLocalIEDriverFromEnum() throws Exception
    {
        WebDriver _driver = DriverFactory.getLocalDriver(DriverFactory.Browser.IE);
        assert(_driver instanceof InternetExplorerDriver);
        _driver.quit();
    }

    //@Test
    public void createRemoteWebDriver() throws Exception
    {
        //Needs a valid grid URL to verify
        WebDriver _driver = DriverFactory.getRemoteDriver(new MutableCapabilities(), "https://www.google.com");
        assert(_driver instanceof RemoteWebDriver);
        _driver.quit();
    }
}
