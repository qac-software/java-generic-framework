package qac;

import com.qaconsultants.config.Config;
import com.qaconsultants.config.TOMLParser;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class TestConfig
{
    private static final String mConfigLocation = "src/test/resources/Config.toml";

    @Test
    public void testConfigPropertyAccess() throws Exception
    {
        Config.load(new TOMLParser(new File(mConfigLocation)));
        Config _config = Config.getInstance();
        assertEquals(_config.project.name, "Project Name");
        assertEquals(_config.pathing.output, "./output/");
        assertEquals(_config.capabilities.get("url"), "TEST URL");
    }

    @Test
    public void testConfigFieldSet() throws Exception
    {
        Config.load(new TOMLParser(new File(mConfigLocation)));
        Config _config = Config.getInstance();
        assertEquals(_config.project.name, "Project Name");

        _config.setSectionField("project", "name", "NEW VALUE");
        assertEquals(_config.project.name, "NEW VALUE");
    }

    @Test
    public void testTOMLParser() throws Exception
    {
        Config.load(new TOMLParser(new File(mConfigLocation)));
        Config _config = Config.getInstance();
        assertNotNull(_config);
    }

    @Test(expected= FileNotFoundException.class)
    public void testTOMLParserNoFile() throws Exception
    {
        Config.load(new TOMLParser(new File("src/test/resources/NotReal.toml")));
        Config _config = Config.getInstance();
    }

    @Test(expected=FileNotFoundException.class)
    public void testTOMLParserDirectory() throws Exception
    {
        Config.load(new TOMLParser(new File("src/test/resources/")));
        Config _config = Config.getInstance();
    }

    @Test(expected=NullPointerException.class)
    public void testTOMLParserNull() throws Exception
    {
        Config.load(new TOMLParser(null));
        Config _config = Config.getInstance();
    }
}
