package com.qaconsultants.config;

import com.moandjiezana.toml.Toml;

import java.io.File;
import java.io.FileNotFoundException;

/**
 *
 * TOMLParser is a concrete implementation of Parser
 *
 * Utilizes Toml4J to parse the supplied file into its java class representation
 *
 */
public class TOMLParser extends Parser<Config> {

    private File mFile;

    /**
     * Constructor
     *
     * @param _file Path to TOML config file
     */
    public TOMLParser(File _file) {
        mFile = _file;
    }


    @Override
    Config parse() throws Exception {
        if (mFile.isFile())
            return new Toml().read(mFile).to(Config.class);
        else
            throw new FileNotFoundException("The supplied File does not exist or the path is incorrect");
    }

}
