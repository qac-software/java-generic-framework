package com.qaconsultants.keywords.validation;

import com.qaconsultants.keywords.Keyword;
import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebElement;

import java.util.function.Predicate;


/**
 *
 * Concrete implementation of Keyword responsible for validating various aspects of a WebElement
 * Utilizes Predicates for inline extensibility and lambda support.
 *
 */
public class ValidateElement extends Keyword<ValidationStrategy, WebElement> {

    @Override
    public boolean Execute(ValidationStrategy _strategy, WebElement _ele) {
        try {
            return _strategy.test(_ele);
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the ValidateElement keyword", e);
            throw e;
        }
    }

    //TODO: Implement dynamic waits for some strategies
    public static ValidationStrategy getTextEqualsStrategy(String _expected){
        return (WebElement e) -> e.getText().equals(_expected);
    }

    public static ValidationStrategy getTextContainsStrategy(String _expected){
        return (WebElement e) -> e.getText().contains(_expected);
    }

    public static ValidationStrategy getAttributeEqualsStrategy(String _attr, String _expected){
        return (WebElement e) -> e.getAttribute(_attr).equals(_expected);
    }

    public static ValidationStrategy getElementEnabledStrategy(boolean _positive){
        return (WebElement e) -> e.isEnabled() == _positive;
    }

    public static ValidationStrategy getElementVisibleStrategy(boolean _positive){
        return (WebElement e) -> e.isDisplayed() == _positive;
    }
}

