package com.qaconsultants.keywords.validation;

import org.openqa.selenium.WebElement;

import java.util.function.Predicate;

public interface ValidationStrategy extends Predicate<WebElement> {
}
