package tests.examples;

import com.qaconsultants.junit.QACTest;
import io.qameta.allure.*;
import io.qameta.allure.junit4.Tag;
import org.junit.*;
import com.qaconsultants.misc.DriverFactory;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import tests.examples.pages.LandingPage;
import tests.examples.pages.LoginPage;

import java.net.URL;
import java.util.function.Predicate;

/**
 *
 * ExampleTest is an example of a valid JUnit Suite using a Page Object Model.
 * Each internal method annotated with @Test is considered its own test case.
 *
 * This class is included in ExampleSuiteCollection which itself is fed to the JUnitCore in App.java to begin test execution.
 *
 * JUnit Annotaton Timeline:
 * @BeforeClass
 * @Before
 * @Test
 * @After
 * @AfterClass
 */
public class ExampleTest extends QACTest {
    private WebDriver mWebDriver;


    /**
     * Methods annotated with the @Before annotation are run prior to each method marked with the @Test annotation
     * <p>
     * This is typically where you will instantiate your WebDriver.
     * Each test should use a clean environment to test against to ensure no outside interference in testing.
     */
    @Before
    public void setUp() throws Exception {
        mWebDriver = DriverFactory.getLocalDriver("CHROME");
    }

    /**
     * Methods annotated with the @After annotation are run after each method marked with the @Test annotation
     * <p>
     * This is typically where you will clean up and close your WebDriver.
     * It is important to close the WebDriver after your test has completed,
     * otherwise you may have silent running processes long after a completed execution.
     */
    @After
    public void tearDown() {
        mWebDriver.close();
        mWebDriver.quit();
    }

    /**
     * Our custom Page object, LoginPage, also has access to the underlying functionality of the abstract Page type.
     * This gives the Page access to common functionality of all pages such as 'inputText', 'clickElement', and data validation methods.
     * <p>
     * To stream-line the test creation process, these functions return the same instance of the page to allow for method chaining.
     * <p>
     * You are not restricted to these methods, as showcased above.
     */
    @Test
    @Description("Sign in using chained methods of a Page object")
    public void SignInWithChainedMethods() throws Exception {
        LoginPage _page = new LoginPage(mWebDriver);

        _page.navigate().inputText(_page.usernameField, "qactest365@gmail.com")
                .clickElement(_page.usernameSubmit)
                .waitForTime(3)
                .inputText(_page.passwordField, "qactest12")
                .submit()
                .validateTitle()
                .validatePage(LandingPage.class);
    }

    /**
     * Our custom Page object, LoginPage, has custom functionality in the form of additional methods.
     * Using these new methods, we will login to the page.
     * <p>
     * This is to showcase that the classes can be as simplistic or complex as the user defines them.
     * A simple method of 'login', in this case, is capable of performing all the actions necessary
     * by only supplying it with a username and password.
     */
    @Test
    @Description("Sign in using a new method of a subclass of Page object")
    public void SignInWithSubclassMethod() throws Exception {
        (new LoginPage(mWebDriver)).navigate()
                .login("qactest365@gmail.com", "qactest12");
    }

    /**
     * An example of a test utilizing Allure annotations to provide the report with more information
     * pertaining to this specific test.
     * <p>
     * @Story is used to delimit the Story that this test is associated with
     * @Issue is used to link this test with a particular issue # or name
     * @Severity is used to signify the severity of this tests passing/failing to the system under test
     * @Description is used to give the end-user an idea of what the test is accomplishing.
     * <p>
     * More annotations are available such as @Feature, @Step for BDD and various others.
     */
    @Test
    @Story("Test Story")
    @Issue("Test Issue")
    @Severity(SeverityLevel.NORMAL)
    @Description("This is a test description")
    public void TestAllureAnnotations(){
        System.out.println("Testing Allure Annotations");
    }
}
