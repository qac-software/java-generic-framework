package tests.examples;

import com.qaconsultants.junit.QACTest;
import com.qaconsultants.misc.DriverFactory;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import tests.examples.pages.LandingPage;
import tests.examples.pages.LoginPage;

/**
 * COPY OF EXAMPLETEST FOR TESTING PARALLELISM
 */
public class ExampleTest2 extends QACTest {
    private WebDriver mWebDriver;


    /**
     * Methods annotated with the @Before annotation are run prior to each method marked with the @Test annotation
     * <p>
     * This is typically where you will instantiate your WebDriver.
     * Each test should use a clean environment to test against to ensure no outside interference in testing.
     */
    @Before
    public void setUp() throws Exception {
        mWebDriver = DriverFactory.getLocalDriver("CHROME");
    }

    /**
     * Methods annotated with the @After annotation are run after each method marked with the @Test annotation
     * <p>
     * This is typically where you will clean up and close your WebDriver.
     * It is important to close the WebDriver after your test has completed,
     * otherwise you may have silent running processes long after a completed execution.
     */
    @After
    public void tearDown() {
        mWebDriver.close();
        mWebDriver.quit();
    }

    /**
     * Our custom Page object, LoginPage, has custom functionality in the form of additional methods.
     * Using these new methods, we will login to the page.
     * <p>
     * This is to showcase that the classes can be as simplistic or complex as the user defines them.
     * A simple method of 'login', in this case, is capable of performing all the actions necessary
     * by only supplying it with a username and password.
     */
    @Test
    public void SignInWithSubclassMethod2() throws Exception {
        (new LoginPage(mWebDriver)).navigate()
                .login("qactest365@gmail.com", "qactest12");
    }

    /**
     * Our custom Page object, LoginPage, also has access to the underlying functionality of the abstract Page type.
     * This gives the Page access to common functionality of all pages such as 'inputText', 'clickElement', and data validation methods.
     * <p>
     * To stream-line the test creation process, these functions return the same instance of the page to allow for method chaining.
     * <p>
     * You are not restricted to these methods, as showcased above.
     */
    @Test
    public void SignInWithChainedMethods2() throws Exception {
        LoginPage _page = new LoginPage(mWebDriver);

        _page.navigate().inputText(_page.usernameField, "qactest365@gmail.com")
                .clickElement(_page.usernameSubmit)
                .waitForTime(3)
                .inputText(_page.passwordField, "qactest12")
                .submit()
                .validateTitle()
                .validatePage(LandingPage.class);
    }
}
